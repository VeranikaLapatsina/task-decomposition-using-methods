class GSDLCM {
    //метод нахождения наибольшего общего делителя
    public static void main(String[] args) {
        int n1 = 72;
        int n2 = 120;
        int gcd = 1;

        int i;
        for(i = 1; i <= n1 && i <= n2; ++i) {
            if (n1 % i == 0 && n2 % i == 0) {
                gcd = i;
            }
        }

        System.out.printf("GSD: %d, ", gcd);
        //метод нахождения наименьшего общего кратного двух натуральных чисел
        for(i = 1; i <= n1 && i <= n2; ++i) {
            if (n1 % i == 0 && n2 % i == 0) {
                gcd = i;
            }
        }
        i = n1 * n2 / gcd;
        System.out.printf("LCM: %d.", i);}}
